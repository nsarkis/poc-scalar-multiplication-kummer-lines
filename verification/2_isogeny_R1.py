from sage.all import randint, next_prime, EllipticCurve, GF


# Theorem 3.7
def isog_R1(x, z, a1p, b1p):
    """Computes the image of (x : z) by the 2-isogeny
    with kernel R1 where R1p = (a1p : b1p) is a
    4-torsion point above R1"""
    a1 = a1p + b1p
    b1 = a1p - b1p

    u = x + z
    v = x - z
    x2 = (b1 * u + a1 * v) ** 2
    z2 = (b1 * u - a1 * v) ** 2

    return a1p**2, b1p**2, x2, z2


p = next_prime(randint(2**63, 2**64))
F = GF(p)

n = 0
c = 0
attempts = 1000
flag = True
print("Verifying 2-isogeny with kernel R1")
while n < attempts and flag:
    A1, B1 = F.random_element(), F.random_element()
    AM1 = -A1 / B1 - B1 / A1
    E1 = EllipticCurve(F, [0, AM1, 0, 1, 0])

    O1 = E1(0)
    T1 = E1(0, 0, 1)
    R1 = E1(A1, 0, B1)  # Extra 2-torsion point
    S1 = E1(B1, 0, A1)

    tmp = R1.division_points(2)
    while len(tmp) == 0:
        A1, B1 = F.random_element(), F.random_element()
        AM1 = -A1 / B1 - B1 / A1
        E1 = EllipticCurve(F, [0, AM1, 0, 1, 0])

        O1 = E1(0)
        T1 = E1(0, 0, 1)
        R1 = E1(A1, 0, B1)  # Extra 2-torsion point
        S1 = E1(B1, 0, A1)

        tmp = R1.division_points(2)

    a1p, b1p = tmp[0][0], tmp[0][2]

    g = E1.isogeny(R1)
    cod = g.codomain()

    P = E1.random_point()

    A2, B2, x, z = isog_R1(P[0], P[2], a1p, b1p)

    AM2 = -A2 / B2 - B2 / A2
    E2 = EllipticCurve(F, [0, AM2, 0, 1, 0])

    O2 = E2(0)
    T2 = E2(0, 0, 1)
    R2 = E2(A2, 0, B2)  # Extra 2-torsion point
    S2 = E2(B2, 0, A2)

    if E2.is_isomorphic(cod):
        phi = cod.isomorphism_to(E2)
        img = phi(g(P)) + R2
        flag = img[0] / img[2] == x / z

    # In case we end up on the twist
    else:
        c += 1
        tw = (x / z) ** 3 + AM2 * (x / z) ** 2 + (x / z)
        E2t = EllipticCurve(F, [0, AM2 / tw, 0, 1 / tw**2, 0])

        O2t = E2t(0)
        T2t = E2t(0, 0, 1)
        R2t = E2t(A2, 0, B2 * tw)
        S2t = E2t(B2, 0, A2 * tw)

        phi = cod.isomorphism_to(E2t)
        img = phi(g(P)) + R2t
        flag = img[0] / img[2] == x / (z * tw)

    n += 1

print(f"Correct codomain for {n} attemps: {flag}")
print(f"Number of times where we land on the twist: {c}")
