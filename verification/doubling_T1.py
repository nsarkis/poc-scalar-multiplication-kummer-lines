from sage.all import randint, next_prime, EllipticCurve, GF


# Algorithm 1
def dbl_translation(x, z, A1, B1):
    """Computes 2.(x : z) + R1 where R1 = (A1 : B1)
    is an extra 2-torsion point"""
    A2 = A1 + B1
    B2 = A1 - B1

    u = B2 * (x + z) ** 2
    v = A2 * (x - z) ** 2
    x2 = B1 * (u + v) ** 2
    z2 = A1 * (u - v) ** 2

    return (x2, z2)


p = next_prime(randint(2**63, 2**64))
F = GF(p)

n = 0
attempts = 1000
flag = True
print("Verifying doubling")
while n < attempts and flag:
    A1, B1 = F.random_element(), F.random_element()
    AM = -A1 / B1 - B1 / A1
    E = EllipticCurve(F, [0, AM, 0, 1, 0])

    O = E(0)
    T = E(0, 0, 1)
    R = E(A1, 0, B1)  # Extra 2-torsion point
    S = E(B1, 0, A1)

    P = E.random_point()
    quasidbl = 2 * P + R
    x, z = dbl_translation(P[0], P[2], A1, B1)
    flag = quasidbl[0] / quasidbl[2] == x / z

    n += 1

print(f"Formulas matched for {n} attemps: {flag}")
