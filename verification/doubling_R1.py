from sage.all import randint, next_prime, EllipticCurve, GF


# Algorithm 2
def dbl_translation(x, z, a1p, b1p):
    """Computes 2.(x : z) + R1 where R1p = (a1p : b1p)
    is a 4-torsion point above R1"""
    a1 = a1p + b1p
    b1 = a1p - b1p

    u = b1 * (x + z)
    v = a1 * (x - z)
    w = (u + v) ** 2
    t = (u - v) ** 2

    u = (w + t) ** 2
    v = (w - t) ** 2

    x2 = B1 * u
    z2 = A1 * (u - v)

    return (x2, z2)


p = next_prime(randint(2**63, 2**64))
F = GF(p)

n = 0
attempts = 1000
flag = True
print("Verifying doubling")
while n < attempts and flag:
    A1, B1 = F.random_element(), F.random_element()
    AM = -A1 / B1 - B1 / A1
    E = EllipticCurve(F, [0, AM, 0, 1, 0])

    O = E(0)
    T = E(0, 0, 1)
    R = E(A1, 0, B1)  # Extra 2-torsion point
    S = E(B1, 0, A1)

    tmp = R.division_points(2)
    while len(tmp) == 0:
        A1, B1 = F.random_element(), F.random_element()
        AM = -A1 / B1 - B1 / A1
        E = EllipticCurve(F, [0, AM, 0, 1, 0])

        O = E(0)
        T = E(0, 0, 1)
        R = E(A1, 0, B1)  # Extra 2-torsion point
        S = E(B1, 0, A1)

        tmp = R.division_points(2)

    a1p, b1p = tmp[0][0], tmp[0][2]

    P = E.random_point()
    quasidbl = 2 * P + R
    x, z = dbl_translation(P[0], P[2], a1p, b1p)
    flag = quasidbl[0] / quasidbl[2] == x / z

    n += 1

print(f"Formulas matched for {n} attemps: {flag}")
