from sage.all import randint, next_prime, EllipticCurve, GF


# Theorem 3.10
def isog_T1_8_tors(x, z, r, s):
    """Computes the image of (x : z) by the 2-isogeny
    with kernel T1 where T1t = (r : s) is a 8-torsion
    point above T1p = (1 : 1) itself above T1"""
    gamma = 4 * r * s
    delta = (r - s) ** 2

    u = (x + z) ** 2
    v = (x - z) ** 2
    x2 = gamma * v
    z2 = delta * (u - v)

    return -delta, gamma, x2, z2


p = next_prime(randint(2**63, 2**64))
F = GF(p)

n = 0
c = 0
attempts = 1000
flag = True
print("Verifying 2-isogeny with kernel T1 (extra 8-torsion)")
while n < attempts and flag:
    flag2 = True
    while flag2:
        A1, B1 = F.random_element(), F.random_element()
        AM1 = -A1 / B1 - B1 / A1
        E1 = EllipticCurve(F, [0, AM1, 0, 1, 0])

        O1 = E1(0)
        T1 = E1(0, 0, 1)

        if E1.is_x_coord(F(1)):
            T1p = E1.lift_x(F(1))

            tmp = T1p.division_points(2)
            flag2 = len(tmp) == 0
        else:
            flag2 = True

    r, s = tmp[0][0], tmp[0][2]

    g = E1.isogeny(T1)
    cod = g.codomain()

    P = E1.random_point()

    A2, B2, x, z = isog_T1_8_tors(P[0], P[2], r, s)

    AM2 = -A2 / B2 - B2 / A2
    E2 = EllipticCurve(F, [0, AM2, 0, 1, 0])

    O2 = E2(0)
    T2 = E2(0, 0, 1)
    R2 = E2(A2, 0, B2)  # Extra 2-torsion point
    S2 = E2(B2, 0, A2)

    if E2.is_isomorphic(cod):
        phi = cod.isomorphism_to(E2)
        img = phi(g(P))
        flag = img[0] / img[2] == x / z

    # In case we end up on the twist
    else:
        c += 1
        tw = (x / z) ** 3 + AM2 * (x / z) ** 2 + (x / z)
        E2t = EllipticCurve(F, [0, AM2 / tw, 0, 1 / tw**2, 0])

        O2t = E2t(0)
        T2t = E2t(0, 0, 1)
        R2t = E2t(A2, 0, B2 * tw)
        S2t = E2t(B2, 0, A2 * tw)

        phi = cod.isomorphism_to(E2t)
        img = phi(g(P))
        flag = img[0] / img[2] == x / (z * tw)

    n += 1

print(f"Correct codomain for {n} attemps: {flag}")
print(f"Number of times where we land on the twist: {c}")
