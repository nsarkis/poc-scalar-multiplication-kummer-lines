from sage.all import GF, sqrt, EllipticCurve


class Parameters:
    def __init__(self, initial_curve, images):
        self.codomains = [initial_curve]
        self.kernels = []
        self.images = images


# [Eq. 5, DJP14]
def strategy(n, dbl_weight, img_weight):
    """Compute the optimal strategy for a 2**(n+1)-isogeny
    where doubling has a dbl_weight cost and image a img_weight cost
    n will be the length of the strategy"""

    S = {1: []}
    C = {1: 0}

    for i in range(2, n + 2):
        costs = [
            C[i - b] + C[b] + b * dbl_weight + (i - b) * img_weight for b in range(1, i)
        ]
        b = costs.index(min(costs)) + 1
        S[i] = [b] + S[i - b] + S[b]
        C[i] = C[i - b] + C[b] + b * dbl_weight + (i - b) * img_weight

    return S[n + 1]


# Theorem 3.7 + Eq. 7
def cod(a1p, b1p):
    """Computes the codomain of the curve with 4-torsion point
    R1p = (a1p : b1p) above R1, returns d = (d_num : d_den)
    and R2 = (a1ps : b1ps)"""
    a1ps = a1p**2
    b1ps = b1p**2
    d_num = (a1ps - b1ps) ** 2
    d_den = d_num - (a1ps + b1ps) ** 2

    return d_num, d_den, a1ps, b1ps


# Thm 3.7
def img(x, z, a1, b1):
    """Computes the image of (x : z) by the 2-isogeny
    with kernel R1 and (a1 : b1) = (a1p + b1p : a1p - b1p)"""
    u = b1 * (x + z)
    v = a1 * (x - z)

    X = (u + v) ** 2
    Z = (u - v) ** 2

    return X, Z


# Alg. 5
def dbl(x, z, d_num, d_den):
    """Computes 2.(x : z) with the Montgomery constant
    d = (d_num : d_den)"""
    u = (x + z) ** 2
    v = (x - z) ** 2

    t = u - v
    w = d_den * v

    X = u * w
    Z = t * (w + d_num * t)

    return X, Z


# SIDH specification: https://sike.org/files/SIDH-spec.pdf
# Section 1.3.8, p. 17
def isogeny(point, parameters, strategy):
    """Compute the 2**t isogeny from the 2**(t+1) torsion point above R1.
    parameters initially contains the first curve constants and list of
    images to compute"""
    t = len(strategy) + 1
    if t == 1:
        # Computes codomain and appends list of kernels
        a1p, b1p = point
        d_num, d_den, a1ps, b1ps = cod(a1p, b1p)
        parameters.kernels += [point]
        parameters.codomains += [[d_num, d_den, a1ps, b1ps]]

        # Computes every images required and store them
        images = []
        a1 = a1p + b1p
        b1 = a1p - b1p
        for pt in parameters.images:
            x, z = img(pt[0], pt[1], a1, b1)
            images.append((x, z))

        parameters.images = images
        return parameters

    n = strategy[0]
    L = strategy[1 : t - n]
    R = strategy[t - n :]
    d_num, d_den, _, _ = parameters.codomains[-1]

    # Doubling phase
    xT, zT = point
    for _ in range(n):
        xT, zT = dbl(xT, zT, d_num, d_den)

    # Recurse
    parameters.images = [point] + parameters.images
    parameters = isogeny((xT, zT), parameters, L)
    T = parameters.images[0]
    parameters.images = parameters.images[1:]
    parameters = isogeny(T, parameters, R)

    return parameters


# SIKEp434 parameters
p = 0x0002341F271773446CFC5FD681C520567BC65C783158AEA3FDC1767AE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
F = GF((p, 2))
i = sqrt(F(-1))  # F = Fp**2 = Fp[i]

xP_real = 0x00003CCFC5E1F050030363E6920A0F7A4C6C71E63DE63A0E6475AF621995705F7C84500CB2BB61E950E19EAB8661D25C4A50ED279646CB48
xP_imag = 0x0001AD1C1CAE7840EDDA6D8A924520F60E573D3B9DFAC6D189941CB22326D284A8816CC4249410FE80D68047D823C97D705246F869E3EA50
xP = xP_real + i * xP_imag  # Point above R1 = (A1 : B1)
e = 0x000000D8  # Order of P

AM = 6

E = EllipticCurve(F, [0, AM, 0, 1, 0])
P = E.lift_x(xP)
Q = E.random_point()
R = 2 ** (e - 1) * P

# Assume S = 2/3 M, even if the implementation is not adapted for Fp[i] here
dbl_weight = 16  # 4M + 2S = 16/3 M
img_weight = 10  # 2M + 2S = 10/3 M

t = 15  # We compute a 2**t isogeny
ker = (
    2 ** (e - t - 1) * P
)  # The kernel is in reality 2.ker, we need a 2**(t+1) torsion point
strat = strategy(t - 1, dbl_weight, img_weight)
parameters = Parameters(
    [2, 1, R[0], R[2]],  # Constants of the first curveh, d = (AM+2)/4 = 2
    [[Q[0], Q[2]]],  # Random point, we want its image
)

# Call the algorithm
isogeny([ker[0], ker[2]], parameters, strat)


# Exploit results
cod_num, cod_den, A, B = parameters.codomains[-1]
coAM = 4 * cod_num / cod_den - 2
coE = EllipticCurve(F, [0, coAM, 0, 1, 0])

f = E.isogeny(2 * ker)
codomain = f.codomain()

print(
    f"The codomain of f by Sage is isomorphic to the one we find: {codomain.is_isomorphic(coE)}"
)
print(f"We have an additional 2-torsion point on the codomain: {coE.is_x_coord(A / B)}")
if codomain.is_isomorphic(coE) and coE.is_x_coord(A / B):
    phi = codomain.isomorphism_to(coE)
    coR = coE.lift_x(A / B)

    # We use translated isogeny, at the last step we have to do a correction
    image = phi(f(Q)) + coR
    alg_image = parameters.images[-1]

    print(f"Correct image: {image[0]/image[2] == alg_image[0]/alg_image[1]}")
