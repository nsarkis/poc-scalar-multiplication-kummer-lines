from sage.all import GF, polygen, EllipticCurve, randint, sum
from sage.rings.finite_rings.integer_mod import IntegerMod_int

from field import p, Fp, nu
import montgomery  # noqa: F401
import hybrid  # noqa: F401
import theta  # noqa: F401

import timeit

# F = Fp**10 = Fp[u, i]
F = GF((p, 10))
t = polygen(F)
u = (t**5 - 2).roots()[0][0]
i = (t**2 + 1).roots()[0][0]

A = 4 * (nu + i) - 2
E = EllipticCurve(F, [0, A, 0, 1, 0])


def to_fp10(x):
    """Converts an element x = [[], []] in a list format to an element of F"""
    return sum([x[0][j] * u**j for j in range(5)]) + i * sum(
        [x[1][j] * u**j for j in range(5)]
    )


def avg(l):
    """Average of a list"""
    n = len(l)
    return sum(l) / n


def stddev(l):
    """Standard dev of a list"""
    m = avg(l)
    return avg([(x - m) ** 2 for x in l])


n = 100
points: list[list[IntegerMod_int]] = []
ints = [randint(2**63, 2**64 - 1) for j in range(n)]

print("Finding points")
while len(points) < n:
    a = [Fp.random_element() for _ in range(5)]
    b = [Fp.random_element() for _ in range(5)]
    c = [Fp.random_element() for _ in range(5)]
    d = [Fp.random_element() for _ in range(5)]
    xP = to_fp10([a, b])
    zP = to_fp10([c, d])
    if E.is_x_coord(xP / zP):
        points.append([[a, b], [c, d]])

print("Montgomery")
timings = timeit.repeat(
    "[montgomery.scalar_mult(ints[j], points[j][0], points[j][1]) for j in range(n)]",
    number=1,
    repeat=100,
    setup="from __main__ import n, ints, points, montgomery",
)
print(f"Timings: {avg(timings):.3f}s ± {stddev(timings):.3f}s")


print("Hybrid")
timings = timeit.repeat(
    "[hybrid.scalar_mult(ints[j], points[j][0], points[j][1]) for j in range(n)]",
    number=1,
    repeat=100,
    setup="from __main__ import n, ints, points, hybrid",
)
print(f"Timings: {avg(timings):.3f}s ± {stddev(timings):.3f}s")

print("Theta")
timings = timeit.repeat(
    "[theta.scalar_mult(ints[j], points[j][0], points[j][1]) for j in range(n)]",
    number=1,
    repeat=100,
    setup="from __main__ import n, ints, points, theta",
)
print(f"Timings: {avg(timings):.3f}s ± {stddev(timings):.3f}s")
