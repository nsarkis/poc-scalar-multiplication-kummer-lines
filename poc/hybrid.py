from field import (
    add_fp10,
    sub_fp10,
    mul_fp10,
    sqr_fp10,
    mul_alpha_fp10,
    mul_alphap1_fp10,
    mul_alpham1_fp10,
)


# Algorithm 4
def diff_add(xP, zP, xQ, zQ, xPmQ, zPmQ):
    """Computes P + Q on the Kummer line with data of P, Q and P - Q"""
    u = mul_fp10(add_fp10(xP, zP), sub_fp10(xQ, zQ))
    v = mul_fp10(sub_fp10(xP, zP), add_fp10(xQ, zQ))

    w = sqr_fp10(add_fp10(u, v))
    t = sqr_fp10(sub_fp10(u, v))

    return mul_fp10(zPmQ, w), mul_fp10(xPmQ, t)


# Algorithm 1
def dbl(xP, zP):
    """Computes 2.P + R1 on the Kummer line"""
    w = sqr_fp10(add_fp10(xP, zP))
    t = sqr_fp10(sub_fp10(xP, zP))

    u = mul_alpham1_fp10(w)
    v = mul_alphap1_fp10(t)
    x2PT = sqr_fp10(add_fp10(u, v))

    w = sqr_fp10(sub_fp10(u, v))
    z2PT = mul_alpha_fp10(w)

    return x2PT, z2PT


# Algorithm 3
def scalar_mult(n, xP, zP):
    """Computes n.P via hybrid ladder"""

    def dbladd(xR, zR, xS, zS, b, xD, zD):
        if b:
            xR, zR = diff_add(xR, zR, xS, zS, xD, zD)
            xS, zS = dbl(xS, zS)
        else:
            xS, zS = diff_add(xR, zR, xS, zS, xD, zD)
            xR, zR = dbl(xR, zR)

        return xR, zR, xS, zS

    xQ = sub_fp10(mul_alpha_fp10(xP), zP)
    zQ = sub_fp10(xP, mul_alpha_fp10(zP))

    D = [[xP, zP], [xQ, zQ]]
    j = 1

    l = int(n).bit_length()
    b = 1
    xR, zR = xP.copy(), zP.copy()
    xS, zS = dbl(xR, zR)
    for i in range(l - 2, -1, -1):
        b = (n >> i) % 2
        xR, zR, xS, zS = dbladd(xR, zR, xS, zS, b, D[j][0], D[j][1])
        j = (j + 1) % 2

    if l % 2 == 0 or b == 0:
        xc = sub_fp10(mul_alpha_fp10(xR), zR)
        zc = sub_fp10(xR, mul_alpha_fp10(zR))
        return xc, zc

    return xR, zR
