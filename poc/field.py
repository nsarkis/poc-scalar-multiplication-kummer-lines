from sage.all import GF

# Appendix C
p = 14859749208866121031
Fp = GF(p)

# AM = - alpha - alpha^-1
# alpha = A1 = 1 + mu*i (A1 in the article)
# d = (AM+2)/4
# d = nu + i
mu = Fp(1141088753069104366)
nu = Fp(400659849698428527)


def mul_fp5(x, y):
    """x*y in Fp**5 with Karatsuba formulas"""
    res = [Fp(0) for _ in range(5)]

    # x = a0 + a1.u^2, y = b0 + b1.u^2
    # a0, b0 in span(1,u)
    # a1, b1 in span(1,u,u^2)

    # a0.b0
    p1 = x[0] * y[0]
    p2 = x[1] * y[1]
    p3 = (x[0] - x[1]) * (y[0] - y[1])

    s1 = p1 + p2 - p3

    res[0] += p1
    res[1] += s1
    res[2] += p2

    res[2] += p1
    res[3] += s1
    res[4] += p2

    # a1.b1
    p1 = x[2] * y[2]
    p2 = x[3] * y[3]
    p3 = x[4] * y[4]
    p4 = (x[2] - x[3]) * (y[2] - y[3])
    p5 = (x[2] - x[4]) * (y[2] - y[4])
    p6 = (x[3] - x[4]) * (y[3] - y[4])

    s1 = p1 + p2 - p4
    s2 = p1 + p2 + p3 - p5
    s3 = p2 + p3 - p6

    res[0] += s1 + s1
    res[1] += s2 + s2
    res[2] += s3 + s3
    res[3] += p3 + p3
    res[4] += p1

    res[0] += s3 + s3
    res[1] += p3 + p3
    res[2] += p1
    res[3] += s1
    res[4] += s2

    # (a1 - a0).(b1 - b0)
    # a1 - a0 = c2 u^2 + c1 u + c0
    # b1 - b0 = d2 u^2 + d1 u + d0
    c2, d2 = x[4], y[4]
    c1, d1 = x[3] - x[1], y[3] - y[1]
    c0, d0 = x[2] - x[0], y[2] - y[0]
    p1 = c0 * d0
    p2 = c1 * d1
    p3 = c2 * d2
    p4 = (c0 - c1) * (d0 - d1)
    p5 = (c0 - c2) * (d0 - d2)
    p6 = (c1 - c2) * (d1 - d2)

    s1 = p1 + p2 - p4
    s2 = p1 + p2 + p3 - p5
    s3 = p2 + p3 - p6

    res[0] -= s3 + s3
    res[1] -= p3 + p3
    res[2] -= p1
    res[3] -= s1
    res[4] -= s2

    return res


def smul_fp5(a, x):
    """a*x in Fp**5 with a in Fp"""
    return [a * b for b in x]


def mul_u_fp5(x):
    """u*x in Fp**5 with u**5 = 2"""
    return [x[4] + x[4]] + [x[:4]]


def add_fp5(x, y):
    """x + y in Fp**5"""
    return [x[i] + y[i] for i in range(5)]


def sub_fp5(x, y):
    """x - y in Fp**5"""
    return [x[i] - y[i] for i in range(5)]


def neg_fp5(x):
    """-x in Fp**5"""
    return [-a for a in x]


def rdm_fp5():
    """Random element in Fp**5"""
    return [Fp.random_element() for _ in range(5)]


def add_fp10(x, y):
    """x + y"""
    return [add_fp5(x[0], y[0]), add_fp5(x[1], y[1])]


def sub_fp10(x, y):
    """x - y"""
    return [sub_fp5(x[0], y[0]), sub_fp5(x[1], y[1])]


def neg_fp10(x):
    """-x in Fp**10"""
    return [neg_fp5(x[0]), neg_fp5(x[1])]


def mul_fp10(x, y):
    """x*y in Fp**10 using Karatsuba formulas"""
    p1 = mul_fp5(x[0], y[0])
    p2 = mul_fp5(x[1], y[1])
    p3 = mul_fp5(sub_fp5(x[0], x[1]), sub_fp5(y[0], y[1]))

    s = add_fp5(p1, p2)

    return [sub_fp5(p1, p2), sub_fp5(s, p3)]


def sqr_fp10(x):
    """x**2 in Fp**10 using Karatsuba formulas"""
    # x = a + ib with i**2 = -1
    # x**2 = (a-b)(a+b) + 2iab
    amb = sub_fp5(x[0], x[1])
    apb = add_fp5(x[0], x[1])
    ab = mul_fp5(x[0], x[1])

    return [mul_fp5(amb, apb), add_fp5(ab, ab)]


def smul_fp10(a, x):
    """a*x in Fp**10 with a in Fp"""
    return [smul_fp5(a, x[0]), smul_fp5(a, x[1])]


def mul_d_fp10(x):
    """d*x in Fp**10 with d = nu + i"""
    p1 = smul_fp5(nu, x[0])
    p2 = smul_fp5(nu, x[1])

    return [sub_fp5(p1, x[1]), add_fp5(x[0], p2)]


def mul_alpha_fp10(x):
    """alpha*x in Fp**10 with alpha = 1 + mu*i"""
    p1 = smul_fp5(mu, x[1])
    p2 = smul_fp5(mu, x[0])

    return [sub_fp5(x[0], p1), add_fp5(x[1], p2)]


def mul_alphap1_fp10(x):
    """(alpha+1)*x in Fp**10 with alpha = 1 + mu*i"""
    p1 = smul_fp5(mu, x[1])
    p2 = smul_fp5(mu, x[0])

    return [sub_fp5(add_fp5(x[0], x[0]), p1), add_fp5(add_fp5(x[1], x[1]), p2)]


def mul_alpham1_fp10(x):
    """(alpha-1)*x in Fp**10 with alpha = 1 + mu*i"""
    p1 = smul_fp5(mu, x[1])
    p2 = smul_fp5(mu, x[0])

    return [neg_fp5(p1), p2]


def rdm_fp10():
    """Random element in Fp**10"""
    return [rdm_fp5(), rdm_fp5()]
