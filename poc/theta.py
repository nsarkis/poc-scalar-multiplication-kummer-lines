from field import (
    add_fp10,
    sub_fp10,
    mul_fp10,
    sqr_fp10,
    mul_alpha_fp10,
    mul_alphap1_fp10,
    mul_alpham1_fp10,
)


# See ...
def diff_add(xP, zP, xQ, zQ, xPmQ, zPmQ):
    """Computes P + Q on the Kummer line with data of P, Q and P - Q"""
    w = mul_fp10(add_fp10(xP, zP), add_fp10(xQ, zQ))
    t = mul_fp10(sub_fp10(xP, zP), sub_fp10(xQ, zQ))

    u = mul_alpham1_fp10(w)
    v = mul_alphap1_fp10(t)
    w = sqr_fp10(add_fp10(u, v))
    t = sqr_fp10(sub_fp10(u, v))

    return mul_fp10(zPmQ, w), mul_fp10(xPmQ, t)


# Algorithm 1
def dbl(xP, zP):
    """Computes 2.P on the Kummer line"""
    w = sqr_fp10(add_fp10(xP, zP))
    t = sqr_fp10(sub_fp10(xP, zP))

    u = mul_alpham1_fp10(w)
    v = mul_alphap1_fp10(t)
    x2PT = sqr_fp10(add_fp10(u, v))

    w = sqr_fp10(sub_fp10(u, v))
    z2PT = mul_alpha_fp10(w)

    return x2PT, z2PT


# Algorithm 6
def scalar_mult(n, xP, zP):
    """Computes n.P via Montgomery ladder"""

    def dbladd(xR, zR, xS, zS, b):
        if b:
            xR, zR = diff_add(xR, zR, xS, zS, xP, zP)
            xS, zS = dbl(xS, zS)
        else:
            xS, zS = diff_add(xR, zR, xS, zS, xP, zP)
            xR, zR = dbl(xR, zR)

        return xR, zR, xS, zS

    l = int(n).bit_length()
    b = 1
    xR, zR = xP.copy(), zP.copy()
    xS, zS = dbl(xR, zR)
    for i in range(l - 2, -1, -1):
        b = (n >> i) % 2
        xR, zR, xS, zS = dbladd(xR, zR, xS, zS, b)

    return xR, zR
