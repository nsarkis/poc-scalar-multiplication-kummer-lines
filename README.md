# PoC Scalar Multiplication Kummer lines

There are three families of scripts:

## Proof of Concept (`poc` folder)

Run `sage -python bench.py` to get timings for Montgomery ladder vs hybrid ladder.

- `field.py`: implementation of `F(p**10) = F(p**5)[i]` and `F(p**5) = F(p)[u]`
  with `i**2 = -1` and `u**5 = 2` as described in the article.
- `montgomery.py`: Montgomery differential addition and doubling formulas
  as well as the Montgomery ladder using the implementation in `field.py`
- `hybrid.py`: Montgomery differential addition and shifted doubling formulas
  as well as the hybrid ladder using the implementation in `field.py`

## Verification scripts (`verification` folder)

Run `sage -python <script>.py` to check the correctness of formulas.
The 5 scripts correspond to the 3 2-isogenies (up to a translation) and
the 2 doubling formulas.

## Computing `2**n`-isogenies (`computing-2n-isogenies.py`)

Run `sage -python computing-2n-isogenies.py` to compute a `2**t`-isogeny
and the image of a random point using our image formulas, using `SIKEp434` parameters.
Also checks that we land on the expected point (up to a translation,
beware that computing the codomain of the Sage isogeny can
get really slow if `t` is too big)
